# openhandx chart

1. 前言
- OpenHandx-chart核心采用了jfreechart，并集成了jfreechart的主要功能。虽然jfreechart的功能很强大，但使用起来也非常复杂。OpenHandx-chart通过xml配置文件或构造config类实例就能生成图表，并统一了区域图、柱状图、折线图、饼图、点图、雷达图配置，数据源统一采用openhandx common的数据模型具备汇总、排序、统计等能力，使用起来更加简单。OpenHandx-chart目的有3个，第一是简化jfreechart使用、第二是增加图表工具的数据加工处理功能、第三是为OpenHandx平台提供图表处理能力。
- 支持的图表有AreaChart（区域图）、BarChart（柱状图）、DialChart（仪表图）、LineChart（折线图）、PieChart（饼图）、PolarChart（雷达图）、RingChart（环状图）、ScatterChart（点图）、WaterfallChart（瀑布图）。2. 名词解释
- 交叉表数据：分析型数据库星型（雪花型）结构的数据
- 列表（多维度）数据：关系型数据库的列表数据

3. 图表分类
- OpenHandx-chart从数据的展现上分为3大类：第一类可以展示两个维度，支持交叉表数据和列表多列数值的数据，第二类可以展示一个维度的RingChart、WaterfallChart只能展示列表一个维度数据，第三类DialChart（仪表图）展示特殊数据。
- **两个维度**
- 支持此类的图表有AreaChart（区域图）、BarChart（柱状图）、LineChart（折线图）、PieChart（饼图）、PolarChart（雷达图）、ScatterChart（点图）。此类的图都有相同的特点：横坐标能表示一个维度，不同的分类（颜色）表示能另一个维度。
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115347_064e9618_1878748.jpeg "area.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115400_71d47272_1878748.jpeg "bar.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115415_9b63d25b_1878748.jpeg "line.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115426_47e60ebf_1878748.jpeg "pie.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115440_91171db9_1878748.jpeg "polar.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115452_c8edd7cd_1878748.jpeg "point.JPG")
- **单维度**
- 支持此类的图表有RingChart（环状图）、WaterfallChart（瀑布图），此类的图都只能展示一个维度
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115528_1009c8b0_1878748.jpeg "ring.JPG")
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115547_53085998_1878748.jpeg "water.JPG")
- **特殊**
- 支持此类的图表有DialChart（仪表图）。仪表图里有最小值、最大值分别代表数值的下、上限，另外还有绿色、黄色、红色分别代表安全、警戒、危险等状况。指针代表当前值，指针所处的刻度可以看出当前值所处状态。用此图表可很好展现库存、资金、降雨量、水位、温度报警等。
![输入图片说明](https://gitee.com/uploads/images/2018/0417/115614_a85f531e_1878748.jpeg "dial.JPG")






